#!/bin/bash

export REPOSITORY_REDIS="https://bitbucket.org/meshstack/deployment-scripts-redis/raw/HEAD/redis"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_REDIS/redis-template.sh --no-cache
chmod +x redis-template.sh
./redis-template.sh -n 15 -p evoila -e openstack
