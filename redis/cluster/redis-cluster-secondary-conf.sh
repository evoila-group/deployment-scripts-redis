#!/bin/bash
echo "####### Running secondary node configuration #######"

echo "instance_id = ${INSTANCE_ID}"

monit stop redis

echo "Stopping Redis Instance for Cluster Configuration"

sleep 10

monit summary

echo "Adding Cluster Configuration to redis.conf"
echo "cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
appendonly yes" >> /etc/redis/redis.conf

monit start redis

echo "Starting Redis Instance after Cluster Configuration"

sleep 10

monit summary

redis-cli -h localhost -p 6379 -a ${REDIS_PASSWORD} CLUSTER MEET ${PRIMARY_IP} 6379

if [ "$INSTANCE_ID" = '2' ]; then
  for slot in {5501..11000}; do redis-cli -h localhost -a ${REDIS_PASSWORD} -p 6379 CLUSTER ADDSLOTS $slot; done;
fi

if [ "$INSTANCE_ID" = '3' ]; then
  for slot in {11001..16383}; do redis-cli -h localhost -a ${REDIS_PASSWORD} -p 6379 CLUSTER ADDSLOTS $slot; done;
fi

### Slave Secondary
echo "Adding Cluster Slave Configuration to redis-slave.conf"
echo "port 7379
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
masterauth ${REDIS_PASSWORD}
appendonly yes" >> /etc/redis/redis-slave.conf

echo "Starting Cluster Slave"
redis-server /etc/redis/redis-slave.conf &

echo "Joining Cluster Slave"
redis-cli -h localhost -p 6379 -a ${REDIS_PASSWORD} CLUSTER MEET 0.0.0.0 7379


# adding file for the set of scripts for checking if cluster is configured
mkdir -p $CHECK_PATH/
chmod 700 -R  $CHECK_PATH/
echo "If this file exists, initialy a cluster was configured." > $CHECK_PATH/secondary-server
