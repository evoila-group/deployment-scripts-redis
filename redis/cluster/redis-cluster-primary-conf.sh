#!/bin/bash
monit stop redis

echo "Stopping Redis Instance for Cluster Configuration"

sleep 10

monit summary

echo "Adding Cluster Configuration to redis.conf"
echo "cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
appendonly yes" >> /etc/redis/redis.conf

monit start redis

echo "Starting Redis Instance after Cluster Configuration"

sleep 10

monit summary

for slot in {0..5500}; do redis-cli -h localhost -a ${REDIS_PASSWORD} -p 6379 CLUSTER ADDSLOTS $slot; done;

### Slave Primary
echo "Adding Cluster Slave Configuration to redis-slave.conf"
echo "port 7379
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
masterauth ${REDIS_PASSWORD}
appendonly yes" >> /etc/redis/redis-slave.conf

echo "Starting Cluster Slave"
redis-server /etc/redis/redis-slave.conf &

echo "Joining Cluster Slave"
redis-cli -h localhost -p 6379 -a ${REDIS_PASSWORD} CLUSTER MEET 0.0.0.0 7379

echo "Starting Replication of Slave"
redis-cli -h localhost -p 6379 -a ${REDIS_PASSWORD} CLUSTER NODES | grep master > node.txt
sed -i "s/\s.*$//" "node.txt"

export NODE_ID=$( cat node.txt )
redis-cli -h localhost -p 7379 -a ${REDIS_PASSWORD} CLUSTER REPLICATE $NODE_ID


# adding file for the set of scripts for checking if cluster is configured
mkdir -p $CHECK_PATH/
chmod 700 -R  $CHECK_PATH/
echo "If this file exists, initialy a cluster was configured." > $CHECK_PATH/primary-server
